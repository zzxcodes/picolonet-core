package main

import (
	"github.com/sirupsen/logrus"
	"os/exec"
	"sync"
)

// todo: change these
const nodeId = "test"
const nodeAddr = "testIpAddr"
const clusterId = "clusterId"

func main() {
	logrus.Info("A walrus appears")
	//go picolo.ScheduleSelfUpdater()
	//picolo.InitAppWithServiceAccount()
	//picolo.Register(clusterId, nodeId, nodeAddr)
	//picolo.ThrowFlare(nodeId)

	stores := make([]string, 3)
	var wg sync.WaitGroup
	wg.Add(1)
	go spawnCdb(&wg, &stores, "", "data/node1")

	wg.Add(1)
	go spawnCdb(&wg, &stores, "", "data/node2")

	wg.Wait()
}

func spawnCdb(wg *sync.WaitGroup, stores *[]string, join string, store string) {
	defer wg.Done()
	//var stdoutBuf, stderrBuf bytes.Buffer
	cmd := exec.Command("bin/cockroach", "start", "--store="+store, "--port=0", "--http-port=0", "--insecure", "--background")
	if err := cmd.Run(); err != nil {
		logrus.Fatalf("Command %s failed with %s\n", cmd.Args, err)
	}
	logrus.Info("The walrus flies")
}