package picolo

import (
	"cloud.google.com/go/firestore"
	"context"
	"github.com/sirupsen/logrus"
	"google.golang.org/genproto/googleapis/type/latlng"
)

var location = &latlng.LatLng{Latitude: 9, Longitude: 179}

func ThrowFlare(nodeId string) error {
	client, err := FB_APP.Firestore(context.Background())
	if err != nil {
		logrus.Fatalf("Error initializing database client: %v", err)
		return err
	}
	defer client.Close()

	_, err = client.Collection("flares").Doc(nodeId).Set(context.Background(), map[string]interface{}{
		"lastFired": firestore.ServerTimestamp,
		"location":  location,
	}, firestore.MergeAll)

	if err != nil {
		logrus.Errorf("Error throwing flare: %v", err)
		return err
	}
	return err
}
