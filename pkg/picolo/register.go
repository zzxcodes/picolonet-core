package picolo

import (
	"cloud.google.com/go/firestore"
	"context"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

func Register(clusterId string, nodeId string, nodeAddr string) error {
	client, err := FB_APP.Firestore(context.Background())
	if err != nil {
		logrus.Fatalf("Error initializing database client: %v", err)
		return err
	}
	defer client.Close()

	nodes := make(map[string]interface{})
	//check if cluster already exists and get nodes if it does
	dsnap, err := client.Collection("clusters").Doc(clusterId).Get(context.Background())
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			// do nothing, the cluster doesn't exist
		} else {
			logrus.Errorf("Error occurred while fetching document: %v", err)
			return err
		}
	}
	if dsnap.Exists() {
		m := dsnap.Data()
		existingNodes, ok := m["nodes"].(map[string]interface{})
		if !ok {
			// Can't assert, handle error.
			logrus.Error("type conversion failed")
		}
		nodes = existingNodes
	}
	nodes[nodeId] = nodeAddr
	_, err = client.Collection("clusters").Doc(clusterId).Set(context.Background(), map[string]interface{}{
		"createdAt": firestore.ServerTimestamp,
		"nodes":     nodes,
	}, firestore.MergeAll)

	if err != nil {
		logrus.Errorf("Error registering node: %v", err)
		return err
	}
	return err
}
