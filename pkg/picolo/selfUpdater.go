package picolo

import (
	"github.com/blang/semver"
	"github.com/jasonlvhit/gocron"
	"github.com/rhysd/go-github-selfupdate/selfupdate"
	"github.com/sirupsen/logrus"
	"time"
)

const version = "1.0.4"
const repo = "picolonet/core"
const selfUpdateTime = "13:00"
const selfUpdateTimeZone = "America/Los_Angeles"

func update() error {
	logrus.Info("Running self update")
	selfupdate.EnableLog()
	current := semver.MustParse(version)
	logrus.Infof("Current version is %s", current)
	latest, err := selfupdate.UpdateSelf(current, repo)
	if err != nil {
		logrus.Infof("Error self updating app: %v", err)
		return err
	}

	if current.Equals(latest.Version) {
		logrus.Infof("Current binary is the latest version %s", version)
	} else {
		logrus.Infof("Update successfully done to version %s", latest.Version)
		logrus.Infof("Release notes: %s", latest.ReleaseNotes)
	}
	return nil
}

func ScheduleSelfUpdater() {
	PST, err := time.LoadLocation(selfUpdateTimeZone)
	if err != nil {
		logrus.Info(err)
		return
	}
	gocron.ChangeLoc(PST)
	gocron.Every(1).Day().At(selfUpdateTime).Do(update)
	<-gocron.Start()
}
