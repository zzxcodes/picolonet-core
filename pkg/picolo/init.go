package picolo

import (
	"context"
	"firebase.google.com/go"
	"github.com/sirupsen/logrus"
	"google.golang.org/api/option"
	"os"
)

const SERVICE_CREDS_FILE_ENV = "SERVICE_CREDS_FILE"

var FB_APP *firebase.App

func InitAppWithServiceAccount() *firebase.App {
	credsFile := os.Getenv(SERVICE_CREDS_FILE_ENV)
	if len(credsFile) == 0 {
		logrus.Fatalln("SERVICE_CREDS_FILE_ENV is not set")
	}
	opt := option.WithCredentialsFile(credsFile)
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		logrus.Infof("Error initializing app: %v", err)
		return nil
	}
	FB_APP = app
	return app
}
